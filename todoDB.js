import levelup from "levelup";
import leveldown from "leveldown";

// Setup
var db = levelup(leveldown("./todo"));

// generate incremental id
export const getNextId = () => {
  return new Promise((resolve, reject) => {
    db.createReadStream({
      keys: true,
      values: false,
      limit: 1,
      reverse: true,
    })
      .on("data", (id) => resolve(Number(id) + 1))
      .on("close", () => resolve(1));
  });
};

export const getTodo = (id) => {
  return db.get(id).then((data) => JSON.parse(data));
};

export const putTodo = (id, todo, done) => {
  const value = JSON.stringify({ todo, done });
  return db.put(id, JSON.stringify({ todo, done }));
};

export const deleteTodo = (id) => {
  return db.del(id);
};

export const iterateTodo = (filter, query) => {
  return new Promise((resolve, reject) => {
    var arr = [];

    db.createReadStream()
      .on("data", ({ key, value }) => {
        const data = {
          key: Number(key.toString()),
          ...JSON.parse(value.toString()),
        };

        if (filter === "all") arr.push(data);
        else if (filter === "done" && data.done) arr.push(data);
        else if (filter === "undone" && !data.done) arr.push(data);
      })
      .on("end", () => {
        var newArr = (query
          ? arr.filter(({ todo }) => todo.search(query) !== -1)
          : arr
        ).sort((a, b) => a.key - b.key);

        resolve(newArr);
      });
  });
};

export default db;
