import chalk from "chalk";
import {
  getNextId,
  getTodo,
  putTodo,
  deleteTodo,
  iterateTodo,
} from "./todoDB.js";
import { todoLog } from "./helper.js";

export const add = (todo) =>
  getNextId().then((id) =>
    putTodo(id, todo, false).then(() => todoLog(todo, id, "added to todolist"))
  );

export const done = (id) =>
  getTodo(id).then(({ todo }) =>
    putTodo(id, todo, true).then(() => todoLog(todo, id, "is done"))
  );

export const del = (id) =>
  getTodo(id).then(({ todo }) =>
    deleteTodo(id).then(() => todoLog(todo, id, "deleted from todolist"))
  );

export const show = (cmdObj) =>
  iterateTodo(cmdObj.filter, cmdObj.search).then((arr) =>
    arr.forEach(({ key, todo, done }) => {
      console.log(
        chalk.blue(key),
        "\t",
        done ? chalk.bgYellow(todo) : chalk.yellow(todo)
      );
    })
  );
