#!/usr/bin/env node
import program from "commander";
import Promise from "bluebird";
import { add, done, del, show } from "./action.js";

//Setup
program.version("0.0.1");

// Commands
program
  .command("add <todo>")
  .description("add todo to the todo list")
  .action(add);

program
  .command("done <todoId>")
  .description("done todo with todo id")
  .action(done);

program
  .command("delete <todoId>")
  .description("delete todo with todo id")
  .action(del);

program
  .command("show")
  .option(
    "-f, --filter <filter>",
    "choose one of all|done|undone filter",
    (filter) =>
      filter === "done" || filter === "undone" || filter === "all"
        ? filter
        : "all",
    "all"
  )
  .option("-s, --search <query>", "select todos that contain search query")
  .description("show todos")
  .action(show);

program.parse(process.argv);
