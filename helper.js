import chalk from "chalk";

export const todoLog = (todo, id, str) => {
  console.log(
    "todo",
    chalk.yellow(`"${todo}"`),
    "with id number",
    chalk.blue(id),
    str
  );
};
